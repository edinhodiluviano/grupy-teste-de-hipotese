#!/usr/bin/env python3


import time
import json
import logging
import datetime

import requests


logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s] [%(name)-15s] [%(levelname)-8s] [func:%(funcName)-12s] - %(message)s'
)
logger = logging.getLogger(__name__)


def get():
    url = 'https://www.facebook.com/'
    start = time.time()
    resp = requests.get(url, timeout=5)
    end = time.time()
    latency = int((end - start) * 1000)
    logger.info(f'{resp.status_code=}; {latency=}')
    return resp.status_code, latency


def save(latency: int):
    file = 'file.jsonl'
    line = json.dumps({
        'timestamp': datetime.datetime.now().isoformat(),
        'latency': latency
    }) + '\n'
    with open(file, 'a') as f:
        f.write(line)


def main():
    n = 0
    while True:
        time.sleep(2)
        try:
            status, latency = get()
        except requests.exceptions.ReadTimeout:
            logger.exception('Timeout')
            time.sleep(10)
        else:
            save(latency)


if __name__ == '__main__':
    main()
